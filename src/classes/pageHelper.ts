export class PageHelper {
    public static getPages(perPage: number, count: number): Array<number> {
        let pages:Array<number> = [];
        for (let i=0; i<count/perPage; i++) {
            if(i>=10) break;
            pages.push(i+1);
        }
        return pages;
    }
}