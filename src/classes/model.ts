import { wrap } from "node-mysql-wrapper";
import * as Env from 'node-env-file';
import {Promise} from "ts-promise";
import * as Sequelize from 'sequelize';

export class Model {

    protected static instance: Model;
    protected config: any;
    protected _model: Sequelize.Sequelize;

    public static model(): Model {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new Model;
        }
        return this.instance;
    }
    constructor() {
        this.config = Env('.env');
        if (this.config.MYSQL_HOST === null || this.config.MYSQL_HOST === undefined) {
            throw new Error('Not registered environment variable MYSQL_HOST');
        }
        if (this.config.MYSQL_USER === null || this.config.MYSQL_USER === undefined) {
            throw new Error('Not registered environment variable MYSQL_USER');
        }
        if (this.config.MYSQL_PASS === null || this.config.MYSQL_PASS === undefined) {
            throw new Error('Not registered environment variable MYSQL_PASS');
        }
        if (this.config.MYSQL_BASE === null || this.config.MYSQL_BASE === undefined) {
            throw new Error('Not registered environment variable MYSQL_BASE');
        }
    }

    /**
     * @returns Sequelize
     */
    public sequelize(): any {
        return new Sequelize(this.config.MYSQL_BASE, this.config.MYSQL_USER, this.config.MYSQL_PASS, {
            host: this.config.MYSQL_HOST,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            },
            define: {
                timestamps: false // true by default
            }
        });
    }

    public connection(): Promise<any> {
        return new Promise((resolve, reject) => {
            resolve(this.sequelize());
        });
    }

    public getConfig() {
        return this.config;
    }

    public save(): Promise<any> {
        let data = this;
        return new Promise((resolve, reject) => {
            this._model.sync().then(() => {
                this._model['create'](data)
                    .then(resolve(data))
                    .catch(error => reject(error));
            });
        });
    }
}