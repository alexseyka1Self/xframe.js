import {BaseComponent} from "./baseComponent";

/**
 * @class BaseWidget
 * @description Компонент генерации и отображения HTML-кода
 */
export class BaseWidget extends BaseComponent {

    private _object: any;
    private _args: any;

    public setObject(obj: any) {
        if (obj) {
            this._object = obj;
        }
    }
    constructor(args: any = {}) {
        super();
        this._args = args;
        if (!this._object) {
            this._object = this;
        }
        this.setProperties();
    }

    protected setProperties() {
        this._args = <Object>this._args;
        Object.keys(this._args).forEach((key, index, values) => {
            this._object[key] = this._args[key];
        });
    }

    public render(){}
    public begin(){}
    public end(){}
}