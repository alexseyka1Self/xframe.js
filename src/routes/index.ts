import {NextFunction, Request, Response, Router} from "express";
import {BaseRoute} from "../classes/baseRoute";
import {Post, PostModel} from "../models/post";
import {Promise} from "ts-promise";
import handlebars = require('hbs');
import Passport = require('passport');
import LocalStrategy = require('passport-local')
import * as passport from "passport";
import {User} from "../models/user";
import {CardWidget} from "../widgets/cardWidget";

/**
 * / route
 *
 * @class User
 */
export class IndexRoute extends BaseRoute {

    /**
     * Create the routes.
     *
     * @class IndexRoute
     * @method create
     * @static
     */
    public static create(router: Router) {
        //add home page route
        router.get("/", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().index(req, res, next);
        });

        router.get("/login", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().loginPage(req, res, next);
        });
        router.post("/login", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().login(req, res, next);
        });

    }

    /**
     * Constructor
     *
     * @class IndexRoute
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * The home page route.
     *
     * @class IndexRoute
     * @method index
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @param next
     * @next {NextFunction} Execute the next method.
     */
    public index(req: Request, res: Response, next: NextFunction) {
        let page: number = req.query.page ? +req.query.page : 1;
        const widget = new CardWidget();
        let options: Object = {
            cardWidget: widget
        };

        let criteria: Object = {};
        // let criteria = {
        //     include: ['Brand'],
        //     limit: productsPerPage,
        //     offset: (productsPerPage * page) - productsPerPage
        // };
        // if (productsAvail && productsAvail != 'all') {
        //     criteria['where'] = {
        //         avalaible_gen: availableGen
        //     };
        // }
        // if (order != null) {
        //     criteria['order'] = [order];
        // }
        const promise = new Promise((resolve, reject) => {
            PostModel.findAndCountAll(criteria).then(products => {
                resolve([<Array<Post>>products.rows, products.count]);
            });
        });
        promise.then(([res, count]) => {
            options['posts'] = <Array<Post>>res;
            options['count'] = count;
            // options['pageNumbers'] = PageHelper.getPages(productsPerPage, count);
        })
            .then(() => {
                this.render(req, res, "index/post-list.hbs", options);
            });
        promise.catch((err) => {
            console.log('Error:', err);
        });
    }

    public loginPage(req: Request, res: Response, next: NextFunction) {
        this.render(req, res, 'index/login-page', {});
    }

    public login(req: Request, res: Response, next: NextFunction) {
        let that = this;
        try {
            passport.authenticate('local',
                function (err, user, info) {
                    console.log('help2');
                    // return err
                    //     ? next(err)
                    //     : user
                    //         ? req['logIn'](user, function(err) {
                    //             return err
                    //                 ? next(err)
                    //                 : that.onAuthorized(req, res, next);
                    //         })
                    //         : res.redirect('/');
                }
            );
        } catch (Error) {
            res.send(Error.message);
        }
    }

    public register(req: Request, res: Response, next: NextFunction) {
        if (req['isAuthenticated']()) {
            this.onAuthorized(req, res, next);
        } else {
            let that = this;
            let user = new User;
            user.login = req['body'].email;
            user.email = req['body'].email;
            user.password = req['body'].password;

            user.save().then(() => {
                req['logIn'](user, function(err) {
                    return err
                        ? next(err)
                        : that.onAuthorized(req, res, next);
                });
            }).catch(err => {
                next(err);
            });
        }
    }

    public logout(req: Request, res: Response, next: NextFunction) {
        req['logout']();
        res.redirect('/');
    }

    private onAuthorized(req: Request, res: Response, next: NextFunction) {
        // res.redirect('/private');
        if (req['isAuthenticated']()) {
            console.log(req['user']);
            res.send('<h1>Hello!</h1>');
        } else {
            res.redirect('/');
        }
    }
}