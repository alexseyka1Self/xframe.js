"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var baseRoute_1 = require("../classes/baseRoute");
var post_1 = require("../models/post");
var ts_promise_1 = require("ts-promise");
var passport = require("passport");
var user_1 = require("../models/user");
var cardWidget_1 = require("../widgets/cardWidget");
/**
 * / route
 *
 * @class User
 */
var IndexRoute = (function (_super) {
    __extends(IndexRoute, _super);
    /**
     * Constructor
     *
     * @class IndexRoute
     * @constructor
     */
    function IndexRoute() {
        return _super.call(this) || this;
    }
    /**
     * Create the routes.
     *
     * @class IndexRoute
     * @method create
     * @static
     */
    IndexRoute.create = function (router) {
        //add home page route
        router.get("/", function (req, res, next) {
            new IndexRoute().index(req, res, next);
        });
        router.get("/login", function (req, res, next) {
            new IndexRoute().loginPage(req, res, next);
        });
        router.post("/login", function (req, res, next) {
            new IndexRoute().login(req, res, next);
        });
    };
    /**
     * The home page route.
     *
     * @class IndexRoute
     * @method index
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @param next
     * @next {NextFunction} Execute the next method.
     */
    IndexRoute.prototype.index = function (req, res, next) {
        var _this = this;
        var page = req.query.page ? +req.query.page : 1;
        var widget = new cardWidget_1.CardWidget();
        var options = {
            cardWidget: widget
        };
        var criteria = {};
        // let criteria = {
        //     include: ['Brand'],
        //     limit: productsPerPage,
        //     offset: (productsPerPage * page) - productsPerPage
        // };
        // if (productsAvail && productsAvail != 'all') {
        //     criteria['where'] = {
        //         avalaible_gen: availableGen
        //     };
        // }
        // if (order != null) {
        //     criteria['order'] = [order];
        // }
        var promise = new ts_promise_1.Promise(function (resolve, reject) {
            post_1.PostModel.findAndCountAll(criteria).then(function (products) {
                resolve([products.rows, products.count]);
            });
        });
        promise.then(function (_a) {
            var res = _a[0], count = _a[1];
            options['posts'] = res;
            options['count'] = count;
            // options['pageNumbers'] = PageHelper.getPages(productsPerPage, count);
        })
            .then(function () {
            _this.render(req, res, "index/post-list.hbs", options);
        });
        promise.catch(function (err) {
            console.log('Error:', err);
        });
    };
    IndexRoute.prototype.loginPage = function (req, res, next) {
        this.render(req, res, 'index/login-page', {});
    };
    IndexRoute.prototype.login = function (req, res, next) {
        var that = this;
        try {
            passport.authenticate('local', function (err, user, info) {
                console.log('help2');
                // return err
                //     ? next(err)
                //     : user
                //         ? req['logIn'](user, function(err) {
                //             return err
                //                 ? next(err)
                //                 : that.onAuthorized(req, res, next);
                //         })
                //         : res.redirect('/');
            });
        }
        catch (Error) {
            res.send(Error.message);
        }
    };
    IndexRoute.prototype.register = function (req, res, next) {
        if (req['isAuthenticated']()) {
            this.onAuthorized(req, res, next);
        }
        else {
            var that_1 = this;
            var user_2 = new user_1.User;
            user_2.login = req['body'].email;
            user_2.email = req['body'].email;
            user_2.password = req['body'].password;
            user_2.save().then(function () {
                req['logIn'](user_2, function (err) {
                    return err
                        ? next(err)
                        : that_1.onAuthorized(req, res, next);
                });
            }).catch(function (err) {
                next(err);
            });
        }
    };
    IndexRoute.prototype.logout = function (req, res, next) {
        req['logout']();
        res.redirect('/');
    };
    IndexRoute.prototype.onAuthorized = function (req, res, next) {
        // res.redirect('/private');
        if (req['isAuthenticated']()) {
            console.log(req['user']);
            res.send('<h1>Hello!</h1>');
        }
        else {
            res.redirect('/');
        }
    };
    return IndexRoute;
}(baseRoute_1.BaseRoute));
exports.IndexRoute = IndexRoute;
//# sourceMappingURL=index.js.map