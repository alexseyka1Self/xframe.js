"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var baseComponent_1 = require("../classes/baseComponent");
/**
 * Class RequestParserComponent
 */
var RequestParserComponent = (function (_super) {
    __extends(RequestParserComponent, _super);
    function RequestParserComponent(app) {
        var _this = _super.call(this) || this;
        //use json form parser middleware
        app.use(bodyParser.json());
        //use query string parser middleware
        app.use(bodyParser.urlencoded({
            extended: true
        }));
        //use cookie parker middleware middleware
        app.use(cookieParser("6g78d6f89g7f9d8gufyufd8"));
        return _this;
    }
    return RequestParserComponent;
}(baseComponent_1.BaseComponent));
exports.RequestParserComponent = RequestParserComponent;
//# sourceMappingURL=requestParserComponent.js.map