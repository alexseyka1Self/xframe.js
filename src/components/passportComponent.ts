import {UserModel} from "../models/user";
import {BaseComponent} from "../classes/baseComponent";
/**
 * Class PassportComponent
 */
export class PassportComponent extends BaseComponent{

    constructor(app) {
        super();
        const passport       = require('passport');
        const LocalStrategy  = require('passport-local').Strategy;
        passport.use(/*'local', */new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password'
        }, function(username, password,done){
            console.log('I am here!');
            // console.log(username, password);
            UserModel.findOne({ where: { login : username} })
                .then(function(user){
                    return user
                        ? password === user.password
                            ? done(null, user)
                            : done(null, false, { message: 'Incorrect password.' })
                        : done(null, false, { message: 'Incorrect username.' });
                })
                .catch(error => done(error));
        }));
        passport.serializeUser(function(user, done) {
            console.log('I am in Serialize!');
            done(null, user);
        });

        passport.deserializeUser(function(user, done) {
            console.log('I am in Deserialize!');
            done(null, user);
        });
        // Passport:
        app.use(passport.initialize());
        app.use(passport.session());
    }
}