import methodOverride = require("method-override");
import {BaseComponent} from "../classes/baseComponent";
/**
 * Class MiddlewareComponent
 */
export class MiddlewareComponent extends BaseComponent{

    constructor(app: any) {
        super();
        app.use(methodOverride());
    }
}