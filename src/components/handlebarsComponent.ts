import handlebars = require('hbs');
import * as path from "path";
import {BaseComponent} from "../classes/baseComponent";
import {BaseWidget} from "../classes/baseWidget";
/**
 * Class HandlebarsComponent
 */
export class HandlebarsComponent extends BaseComponent{

    constructor(app) {
        super();
        let hbs = handlebars.create();
        hbs.registerHelper('ifCond', function (v1, operator, v2, options) {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '!=':
                    return (v1 != v2) ? options.fn(this) : options.inverse(this);
                case '!==':
                    return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        });

        hbs.registerHelper('widget', function (widget: BaseWidget, method: string) {
            if (widget instanceof BaseWidget) {
                if (method == 'begin') {
                    return widget.begin();
                } else if (method == 'end') {
                    return widget.end();
                }
            }
        });

        hbs.registerHelper('round', arg => Math.round(arg));
        app.engine('hbs', hbs.__express);
        app.set("views", path.join(__dirname, "../views/"));
        app.set("view engine", "hbs");
    }
}