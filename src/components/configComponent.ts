import * as express from "express";
import * as path from "path";
import errorHandler = require("errorhandler");
import methodOverride = require("method-override");
import session = require('express-session');

import {ExpressSessionComponent} from "./expressSessionComponent";
import {FlashMessagesComponent} from "./flashMessagesComponent";
import {HandlebarsComponent} from "./handlebarsComponent";
import {PassportComponent} from "./passportComponent";
import {LoggerComponent} from "./loggerComponent";
import {RequestParserComponent} from "./requestParserComponent";
import {MiddlewareComponent} from "./middlewareComponent";
import {ErrorHandlerComponent} from "./errorHandlerComponent";
import {BaseComponent} from "../classes/baseComponent";
/**
 * Class ConfigComponent
 */
export class ConfigComponent extends BaseComponent{

    constructor(app: any) {
        super();
        //add static paths
        app.use(express.static(path.join(__dirname, "../public")));

        /**
         * Connect express-session
         */
        const expressSession = new ExpressSessionComponent(app);
        const flashMessages = new FlashMessagesComponent(app);
        const handlebars = new HandlebarsComponent(app);
        const logger = new LoggerComponent(app);
        const requestParser = new RequestParserComponent(app);
        const middlewares = new MiddlewareComponent(app);
        const errorHandler = new ErrorHandlerComponent(app);
        const passport = new PassportComponent(app);
    }
}