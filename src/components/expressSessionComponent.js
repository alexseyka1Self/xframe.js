"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var model_1 = require("../classes/model");
var MySQLStore = require("express-mysql-session");
var mysql = require("mysql");
var baseComponent_1 = require("../classes/baseComponent");
/**
 * Class ExpressSessionComponent
 */
var ExpressSessionComponent = (function (_super) {
    __extends(ExpressSessionComponent, _super);
    function ExpressSessionComponent(app) {
        var _this = _super.call(this) || this;
        var session = require('express-session');
        app.set('trust proxy', 1); // trust first proxy
        var options = {
            host: model_1.Model.model().getConfig().MYSQL_HOST,
            port: model_1.Model.model().getConfig().MYSQL_PORT,
            user: model_1.Model.model().getConfig().MYSQL_USER,
            password: model_1.Model.model().getConfig().MYSQL_PASS,
            database: model_1.Model.model().getConfig().MYSQL_BASE // Database name
        };
        var connection = mysql.createConnection(options);
        var sessionStore = new MySQLStore({
            checkExpirationInterval: 900000,
            expiration: 86400000,
            createDatabaseTable: true,
            connectionLimit: 1,
            schema: {
                tableName: 'sessions',
                columnNames: {
                    session_id: 'session_id',
                    expires: 'expires',
                    data: 'data'
                }
            }
        }, connection);
        var sessionOpts = {
            secret: '6g78d6f89g7f9d8gufyufd8',
            resave: false,
            saveUninitialized: true,
            store: sessionStore,
            cookie: { secure: true, httpOnly: true, maxAge: 2419200000 } // configure when sessions expires
        };
        app.use(session(sessionOpts));
        return _this;
    }
    return ExpressSessionComponent;
}(baseComponent_1.BaseComponent));
exports.ExpressSessionComponent = ExpressSessionComponent;
//# sourceMappingURL=expressSessionComponent.js.map