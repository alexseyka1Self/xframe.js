import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import {BaseComponent} from "../classes/baseComponent";
/**
 * Class RequestParserComponent
 */
export class RequestParserComponent extends BaseComponent{

    constructor(app: any) {
        super();
        //use json form parser middleware
        app.use(bodyParser.json());

        //use query string parser middleware
        app.use(bodyParser.urlencoded({
            extended: true
        }));

        //use cookie parker middleware middleware
        app.use(cookieParser("6g78d6f89g7f9d8gufyufd8"));
    }
}