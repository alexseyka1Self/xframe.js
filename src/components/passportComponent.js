"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var user_1 = require("../models/user");
var baseComponent_1 = require("../classes/baseComponent");
/**
 * Class PassportComponent
 */
var PassportComponent = (function (_super) {
    __extends(PassportComponent, _super);
    function PassportComponent(app) {
        var _this = _super.call(this) || this;
        var passport = require('passport');
        var LocalStrategy = require('passport-local').Strategy;
        passport.use(/*'local', */ new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password'
        }, function (username, password, done) {
            console.log('I am here!');
            // console.log(username, password);
            user_1.UserModel.findOne({ where: { login: username } })
                .then(function (user) {
                return user
                    ? password === user.password
                        ? done(null, user)
                        : done(null, false, { message: 'Incorrect password.' })
                    : done(null, false, { message: 'Incorrect username.' });
            })
                .catch(function (error) { return done(error); });
        }));
        passport.serializeUser(function (user, done) {
            console.log('I am in Serialize!');
            done(null, user);
        });
        passport.deserializeUser(function (user, done) {
            console.log('I am in Deserialize!');
            done(null, user);
        });
        // Passport:
        app.use(passport.initialize());
        app.use(passport.session());
        return _this;
    }
    return PassportComponent;
}(baseComponent_1.BaseComponent));
exports.PassportComponent = PassportComponent;
//# sourceMappingURL=passportComponent.js.map