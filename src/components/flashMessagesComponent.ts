import {BaseComponent} from "../classes/baseComponent";
/**
 * Class FlashMessagesComponent
 */
export class FlashMessagesComponent extends BaseComponent{

    constructor(app: any) {
        super();
        app.use(require('connect-flash')());
        app.use(function (req, res, next) {
            res.locals.messages = require('express-messages')(req, res);
            next();
        });
    }
}