import * as express from "express";
import {IndexRoute} from "../routes/index";
import {BaseComponent} from "../classes/baseComponent";

/**
 * Class RoutesComponent
 */
export class RoutesComponent extends BaseComponent {

    constructor(app: any) {
        super();
        let router: express.Router;
        router = express.Router();

        //IndexRoute
        IndexRoute.create(router);

        //use router middleware
        app.use(router);
    }
}