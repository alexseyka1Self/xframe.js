import {Model} from "../classes/model";
import * as MySQLStore from "express-mysql-session";
import * as mysql from "mysql";
import {BaseComponent} from "../classes/baseComponent";

/**
 * Class ExpressSessionComponent
 */
export class ExpressSessionComponent extends BaseComponent{

    constructor(app: any) {
        super();
        let session = require('express-session');
        app.set('trust proxy', 1); // trust first proxy
        let options = {
            host: Model.model().getConfig().MYSQL_HOST,// Host name for database connection.
            port: Model.model().getConfig().MYSQL_PORT,// Port number for database connection.
            user: Model.model().getConfig().MYSQL_USER,// Database user.
            password: Model.model().getConfig().MYSQL_PASS,// Password for the above database user.
            database: Model.model().getConfig().MYSQL_BASE// Database name
        };
        let connection = mysql.createConnection(options);
        let sessionStore = new MySQLStore({
            checkExpirationInterval: 900000,// How frequently expired sessions will be cleared; milliseconds.
            expiration: 86400000,// The maximum age of a valid session; milliseconds.
            createDatabaseTable: true,// Whether or not to create the sessions database table, if one does not already exist.
            connectionLimit: 1,// Number of connections when creating a connection pool
            schema: {
                tableName: 'sessions',
                columnNames: {
                    session_id: 'session_id',
                    expires: 'expires',
                    data: 'data'
                }
            }
        }, connection);
        let sessionOpts = {
            secret: '6g78d6f89g7f9d8gufyufd8',
            resave: false,
            saveUninitialized: true,
            store: sessionStore,
            cookie : { secure: true, httpOnly: true, maxAge: 2419200000 } // configure when sessions expires
        };
        app.use(session(sessionOpts));
    }
}