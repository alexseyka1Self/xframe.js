import * as logger from "morgan";
import {BaseComponent} from "../classes/baseComponent";
/**
 * Class LoggerComponent
 */
export class LoggerComponent extends BaseComponent{

    constructor(app) {
        super();
        app.use(logger("dev"));
    }
}