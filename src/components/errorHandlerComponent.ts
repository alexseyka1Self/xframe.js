import * as express from "express";
import errorHandler = require("errorhandler");
import {BaseComponent} from "../classes/baseComponent";
/**
 * Class ErrorHandlerComponent
 */
export class ErrorHandlerComponent extends BaseComponent{

    constructor(app) {
        super();
        //catch 404 and forward to error handler
        app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
            res.status(404);
            console.log(err);
            res.send({title: 'Error!', error: err});
        });

        //error handling
        app.use(errorHandler());
    }
}