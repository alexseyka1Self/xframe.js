import {Model} from '../classes/model';
import * as Sequelize from 'sequelize';

const sequelize = Model.model().sequelize();
export const PostModel = sequelize.define('post', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    text: {
        type: Sequelize.STRING,
        allowNull: false
    },
    url: {
        type: Sequelize.STRING,
    },
});

export class Post extends Model {
    public id: number;
    public title: string;
    public text: string;
    public url: string;

    protected _model = PostModel;
}