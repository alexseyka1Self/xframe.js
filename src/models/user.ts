import {Model} from '../classes/model';
import * as Sequelize from 'sequelize';
import {Promise} from "ts-promise";

const sequelize = Model.model().sequelize();
export const UserModel = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    login: {
        type: Sequelize.STRING,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    lastname: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    phone: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    date_register: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
});

export class User extends Model {
    public id: number;
    public login: string;
    public name: string;
    public lastname: string;
    public phone: string;
    public email: string;
    public date_register: string;
    public password: string;

    protected _model = UserModel;
}