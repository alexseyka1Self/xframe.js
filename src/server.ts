import * as express from "express";
import {RoutesComponent} from "./components/routesComponent";
import {ConfigComponent} from "./components/configComponent";

/**
 * The server.
 *
 * @class Server
 */
export class Server {

    private routes: RoutesComponent;
    private config: ConfigComponent;
    public app: express.Application;

    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
     */
    public static bootstrap(): Server {
        return new Server();
    }

    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {
        this.app = express();
        this.config = new ConfigComponent(this.app);
        this.routes = new RoutesComponent(this.app);
    }
}