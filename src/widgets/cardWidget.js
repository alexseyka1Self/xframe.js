"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var baseWidget_1 = require("../classes/baseWidget");
/**
 * @class CardWidget
 * @description Виджет "Карточка" из Bootstrap4
 */
var CardWidget = (function (_super) {
    __extends(CardWidget, _super);
    function CardWidget(args) {
        if (args === void 0) { args = {}; }
        var _this = _super.call(this, args) || this;
        _this.header = null;
        _this.footer = null;
        _this._output = null;
        _super.prototype.setObject.call(_this, _this);
        _super.prototype.setProperties.call(_this);
        return _this;
    }
    CardWidget.prototype.begin = function () {
        _super.prototype.begin.call(this);
        if (this.header !== null) {
            this._output = "\n                <div class=\"card\">\n                    <div class=\"card-header\">\n                        <h3>" + this.header + "</h3>\n                    </div>\n                    <div class=\"card-block\">\n            ";
        }
        else {
            this._output = "\n                <div class=\"card\">\n                    <div class=\"card-block\">\n            ";
        }
        return this._output;
    };
    CardWidget.prototype.end = function () {
        console.log(this);
        _super.prototype.end.call(this);
        if (this.footer !== null) {
            this._output = "\n                    </div>\n                    <div class=\"card-footer clearfix\">\n                        <div class=\"footer-container\">\n                            " + this.footer + "\n                        </div>\n                    </div>\n                </div>\n            ";
        }
        else {
            this._output = "</div></div>";
        }
        return this._output;
    };
    CardWidget.prototype.render = function () {
        return _super.prototype.render.call(this);
    };
    return CardWidget;
}(baseWidget_1.BaseWidget));
exports.CardWidget = CardWidget;
//# sourceMappingURL=cardWidget.js.map