import {BaseWidget} from "../classes/baseWidget";
/**
 * @class CardWidget
 * @description Виджет "Карточка" из Bootstrap4
 */
export class CardWidget extends BaseWidget {

    public header: string = null;
    public footer: string = null;
    private _output: string = null;


    constructor(args: any = {}) {
        super(args);
        super.setObject(this);
        super.setProperties();
    }

    public begin() {
        super.begin();
        if (this.header !== null) {
            this._output = `
                <div class="card">
                    <div class="card-header">
                        <h3>${this.header}</h3>
                    </div>
                    <div class="card-block">
            `;
        } else {
            this._output = `
                <div class="card">
                    <div class="card-block">
            `;
        }
        return this._output;
    }

    public end() {
        console.log(this);
        super.end();
        if (this.footer !== null) {
            this._output = `
                    </div>
                    <div class="card-footer clearfix">
                        <div class="footer-container">
                            ${this.footer}
                        </div>
                    </div>
                </div>
            `;
        } else {
            this._output = `</div></div>`;
        }
        return this._output;
    }

    public render() {
        return super.render();
    }
}