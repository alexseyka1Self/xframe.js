module.exports = function(grunt) {
    "use strict";

    grunt.rootDir = '';
    grunt.initConfig({
        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        cwd: "./public",
                        src: ["**"],
                        dest: "./dist/public"
                    },
                    {
                        expand: true,
                        cwd: "./views",
                        src: ["**"],
                        dest: "./dist/views"
                    }
                ]
            }
        },
        ts: {
            app: {
                files: [
                    {
                        src: ["src/\*\*/\*.ts", "!src/.baseDir.ts"],
                        dest: "./dist"
                    },
                    {
                        src: ["www.ts"],
                        dest: "./bin"
                    }
                ],
                options: {
                    additionalFlags: "--experimentalDecorators",
                    module: "commonjs",
                    target: "es5",
                    sourceMap: false
                }
            }
        },
        watch: {
            ts: {
                files: ["src/\*\*/\*.ts"],
                tasks: ["ts"]
            },
            views: {
                files: ["views/**/*.hbs"],
                tasks: ["copy"]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-ts");

    grunt.registerTask("default", [
        "copy",
        "ts"
    ]);

};