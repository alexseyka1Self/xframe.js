"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var path = require("path");
var expressSessionComponent_1 = require("./expressSessionComponent");
var flashMessagesComponent_1 = require("./flashMessagesComponent");
var handlebarsComponent_1 = require("./handlebarsComponent");
var passportComponent_1 = require("./passportComponent");
var loggerComponent_1 = require("./loggerComponent");
var requestParserComponent_1 = require("./requestParserComponent");
var middlewareComponent_1 = require("./middlewareComponent");
var errorHandlerComponent_1 = require("./errorHandlerComponent");
var baseComponent_1 = require("../classes/baseComponent");
var ConfigComponent = (function (_super) {
    __extends(ConfigComponent, _super);
    function ConfigComponent(app) {
        var _this = _super.call(this) || this;
        app.use(express.static(path.join(__dirname, "../public")));
        var expressSession = new expressSessionComponent_1.ExpressSessionComponent(app);
        var flashMessages = new flashMessagesComponent_1.FlashMessagesComponent(app);
        var handlebars = new handlebarsComponent_1.HandlebarsComponent(app);
        var logger = new loggerComponent_1.LoggerComponent(app);
        var requestParser = new requestParserComponent_1.RequestParserComponent(app);
        var middlewares = new middlewareComponent_1.MiddlewareComponent(app);
        var errorHandler = new errorHandlerComponent_1.ErrorHandlerComponent(app);
        var passport = new passportComponent_1.PassportComponent(app);
        return _this;
    }
    return ConfigComponent;
}(baseComponent_1.BaseComponent));
exports.ConfigComponent = ConfigComponent;
