"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var index_1 = require("../routes/index");
var baseComponent_1 = require("../classes/baseComponent");
var RoutesComponent = (function (_super) {
    __extends(RoutesComponent, _super);
    function RoutesComponent(app) {
        var _this = _super.call(this) || this;
        var router;
        router = express.Router();
        index_1.IndexRoute.create(router);
        app.use(router);
        return _this;
    }
    return RoutesComponent;
}(baseComponent_1.BaseComponent));
exports.RoutesComponent = RoutesComponent;
