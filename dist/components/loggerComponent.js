"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var logger = require("morgan");
var baseComponent_1 = require("../classes/baseComponent");
var LoggerComponent = (function (_super) {
    __extends(LoggerComponent, _super);
    function LoggerComponent(app) {
        var _this = _super.call(this) || this;
        app.use(logger("dev"));
        return _this;
    }
    return LoggerComponent;
}(baseComponent_1.BaseComponent));
exports.LoggerComponent = LoggerComponent;
