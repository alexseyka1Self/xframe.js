"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var errorHandler = require("errorhandler");
var baseComponent_1 = require("../classes/baseComponent");
var ErrorHandlerComponent = (function (_super) {
    __extends(ErrorHandlerComponent, _super);
    function ErrorHandlerComponent(app) {
        var _this = _super.call(this) || this;
        app.use(function (err, req, res, next) {
            res.status(404);
            console.log(err);
            res.send({ title: 'Error!', error: err });
        });
        app.use(errorHandler());
        return _this;
    }
    return ErrorHandlerComponent;
}(baseComponent_1.BaseComponent));
exports.ErrorHandlerComponent = ErrorHandlerComponent;
