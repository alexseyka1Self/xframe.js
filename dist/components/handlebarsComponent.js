"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var handlebars = require("hbs");
var path = require("path");
var baseComponent_1 = require("../classes/baseComponent");
var baseWidget_1 = require("../classes/baseWidget");
var HandlebarsComponent = (function (_super) {
    __extends(HandlebarsComponent, _super);
    function HandlebarsComponent(app) {
        var _this = _super.call(this) || this;
        var hbs = handlebars.create();
        hbs.registerHelper('ifCond', function (v1, operator, v2, options) {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '!=':
                    return (v1 != v2) ? options.fn(this) : options.inverse(this);
                case '!==':
                    return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        });
        hbs.registerHelper('widget', function (widget, method) {
            if (widget instanceof baseWidget_1.BaseWidget) {
                if (method == 'begin') {
                    return widget.begin();
                }
                else if (method == 'end') {
                    return widget.end();
                }
            }
        });
        hbs.registerHelper('round', function (arg) { return Math.round(arg); });
        app.engine('hbs', hbs.__express);
        app.set("views", path.join(__dirname, "../views/"));
        app.set("view engine", "hbs");
        return _this;
    }
    return HandlebarsComponent;
}(baseComponent_1.BaseComponent));
exports.HandlebarsComponent = HandlebarsComponent;
