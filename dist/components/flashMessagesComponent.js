"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var baseComponent_1 = require("../classes/baseComponent");
var FlashMessagesComponent = (function (_super) {
    __extends(FlashMessagesComponent, _super);
    function FlashMessagesComponent(app) {
        var _this = _super.call(this) || this;
        app.use(require('connect-flash')());
        app.use(function (req, res, next) {
            res.locals.messages = require('express-messages')(req, res);
            next();
        });
        return _this;
    }
    return FlashMessagesComponent;
}(baseComponent_1.BaseComponent));
exports.FlashMessagesComponent = FlashMessagesComponent;
