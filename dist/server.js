"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var routesComponent_1 = require("./components/routesComponent");
var configComponent_1 = require("./components/configComponent");
var Server = (function () {
    function Server() {
        this.app = express();
        this.config = new configComponent_1.ConfigComponent(this.app);
        this.routes = new routesComponent_1.RoutesComponent(this.app);
    }
    Server.bootstrap = function () {
        return new Server();
    };
    return Server;
}());
exports.Server = Server;
