"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PageHelper = (function () {
    function PageHelper() {
    }
    PageHelper.getPages = function (perPage, count) {
        var pages = [];
        for (var i = 0; i < count / perPage; i++) {
            if (i >= 10)
                break;
            pages.push(i + 1);
        }
        return pages;
    };
    return PageHelper;
}());
exports.PageHelper = PageHelper;
