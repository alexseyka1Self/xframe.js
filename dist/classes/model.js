"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Env = require("node-env-file");
var ts_promise_1 = require("ts-promise");
var Sequelize = require("sequelize");
var Model = (function () {
    function Model() {
        this.config = Env('.env');
        if (this.config.MYSQL_HOST === null || this.config.MYSQL_HOST === undefined) {
            throw new Error('Not registered environment variable MYSQL_HOST');
        }
        if (this.config.MYSQL_USER === null || this.config.MYSQL_USER === undefined) {
            throw new Error('Not registered environment variable MYSQL_USER');
        }
        if (this.config.MYSQL_PASS === null || this.config.MYSQL_PASS === undefined) {
            throw new Error('Not registered environment variable MYSQL_PASS');
        }
        if (this.config.MYSQL_BASE === null || this.config.MYSQL_BASE === undefined) {
            throw new Error('Not registered environment variable MYSQL_BASE');
        }
    }
    Model.model = function () {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new Model;
        }
        return this.instance;
    };
    Model.prototype.sequelize = function () {
        return new Sequelize(this.config.MYSQL_BASE, this.config.MYSQL_USER, this.config.MYSQL_PASS, {
            host: this.config.MYSQL_HOST,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            },
            define: {
                timestamps: false
            }
        });
    };
    Model.prototype.connection = function () {
        var _this = this;
        return new ts_promise_1.Promise(function (resolve, reject) {
            resolve(_this.sequelize());
        });
    };
    Model.prototype.getConfig = function () {
        return this.config;
    };
    Model.prototype.save = function () {
        var _this = this;
        var data = this;
        return new ts_promise_1.Promise(function (resolve, reject) {
            _this._model.sync().then(function () {
                _this._model['create'](data)
                    .then(resolve(data))
                    .catch(function (error) { return reject(error); });
            });
        });
    };
    return Model;
}());
exports.Model = Model;
