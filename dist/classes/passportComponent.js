"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var passport = require("passport");
var passportLocal = require("passport-local");
var user_1 = require("../models/user");
var LocalStrategy = passportLocal.Strategy;
passport.serializeUser(function (user, done) {
    done(undefined, user.id);
});
passport.deserializeUser(function (id, done) {
    user_1.UserModel.findById(id)
        .then(function (user) { return done(user, undefined); })
        .catch(function (error) { return done(undefined, error); });
});
passport.use(new LocalStrategy({ usernameField: "email" }, function (email, password, done) {
    user_1.UserModel.findOne({
        where: {
            email: email.toLowerCase()
        }
    }).then(function (user) {
        if (!user) {
            return done(undefined, false, { message: "Email " + email + " not found." });
        }
        user.comparePassword(password, function (err, isMatch) {
            if (err) {
                return done(err);
            }
            if (isMatch) {
                return done(undefined, user);
            }
            return done(undefined, false, { message: "Invalid email or password." });
        });
    }).catch(function (error) {
        if (error) {
            return done(error);
        }
    });
}));
