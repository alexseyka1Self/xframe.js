"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var baseComponent_1 = require("./baseComponent");
var BaseWidget = (function (_super) {
    __extends(BaseWidget, _super);
    function BaseWidget(args) {
        if (args === void 0) { args = {}; }
        var _this = _super.call(this) || this;
        _this._args = args;
        if (!_this._object) {
            _this._object = _this;
        }
        _this.setProperties();
        return _this;
    }
    BaseWidget.prototype.setObject = function (obj) {
        if (obj) {
            this._object = obj;
        }
    };
    BaseWidget.prototype.setProperties = function () {
        var _this = this;
        this._args = this._args;
        Object.keys(this._args).forEach(function (key, index, values) {
            _this._object[key] = _this._args[key];
        });
    };
    BaseWidget.prototype.render = function () { };
    BaseWidget.prototype.begin = function () { };
    BaseWidget.prototype.end = function () { };
    return BaseWidget;
}(baseComponent_1.BaseComponent));
exports.BaseWidget = BaseWidget;
