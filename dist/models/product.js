"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var model_1 = require("../classes/model");
var brand_1 = require("./brand");
var Sequelize = require("sequelize");
var comment_1 = require("./comment");
var sequelize = model_1.Model.model().sequelize();
exports.ProductModel = sequelize.define('product', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    art_num: {
        type: Sequelize.STRING,
    },
    brand_id: {
        type: Sequelize.INTEGER
    },
    avalaible_gen: {
        type: Sequelize.INTEGER
    },
    price_sredn: {
        type: Sequelize.DECIMAL
    },
    kolich: {
        type: Sequelize.INTEGER
    },
    marge: {
        type: Sequelize.INTEGER,
    },
    id_dok_izdelie: {
        type: Sequelize.INTEGER
    },
    koef_merge_1: {
        type: Sequelize.INTEGER
    },
    min_hour: {
        type: Sequelize.INTEGER,
    },
    koef_merge_2: {
        type: Sequelize.INTEGER,
    },
    koef_result: {
        type: Sequelize.INTEGER,
    },
    price_torg: {
        type: Sequelize.DECIMAL,
    },
    pa_art_id: {
        type: Sequelize.INTEGER,
    },
    pa_ga_id: {
        type: Sequelize.INTEGER,
    },
    pa_ga_id_cross: {
        type: Sequelize.INTEGER,
    },
    pa_ga_id_search: {
        type: Sequelize.INTEGER,
    },
});
exports.ProductModel.belongsTo(brand_1.BrandModel, { as: 'Brand', foreignKey: 'brand_id' });
exports.ProductModel.hasMany(comment_1.CommentModel, { as: 'Comments', foreignKey: 'product_id', targetKey: 'id' });
var Product = (function (_super) {
    __extends(Product, _super);
    function Product() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Product;
}(model_1.Model));
exports.Product = Product;
