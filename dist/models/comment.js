"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var model_1 = require("../classes/model");
var Sequelize = require("sequelize");
var sequelize = model_1.Model.model().sequelize();
exports.CommentModel = sequelize.define('comment', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    product_id: {
        type: Sequelize.INTEGER,
    },
    name: {
        type: Sequelize.STRING
    },
    text: {
        type: Sequelize.TEXT
    },
});
var Comment = (function (_super) {
    __extends(Comment, _super);
    function Comment() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._model = exports.CommentModel;
        return _this;
    }
    return Comment;
}(model_1.Model));
exports.Comment = Comment;
