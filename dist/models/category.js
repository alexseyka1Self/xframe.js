"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var model_1 = require("../classes/model");
var sequelize_1 = require("sequelize");
var sequelize = model_1.Model.model().sequelize();
exports.CategoryModel = sequelize.define('category', {
    id: {
        type: sequelize_1.Sequelize.INTEGER,
        primaryKey: true
    },
    name: {
        type: sequelize_1.Sequelize.STRING
    },
    parent_id: {
        type: sequelize_1.Sequelize.INTEGER
    },
    created_at: {
        type: sequelize_1.Sequelize.DATE,
        defaultValue: sequelize_1.Sequelize.NOW
    },
    updated_at: {
        type: sequelize_1.Sequelize.DATE
    },
});
var Category = (function (_super) {
    __extends(Category, _super);
    function Category() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Category;
}(model_1.Model));
exports.Category = Category;
