'use strict';
$(document).ready(function(){

    $(document).on('change', '#selectAvailableOfProducts', function(){
        $(this).parents('form').submit();
    });
    $(document).on('change', '#orderOfProducts', function(){
        $(this).parents('form').submit();
    });
    $(document).on('submit', '#commentForm', function(e){
        let cookieName = 'last-message';
        let cookieOptions = {expires: 300, path: '/'};

        let form = $(this).serializeArray();
        let doSubmit = true;
        let errorBlock = $('#add-comment-errors');
        let errorBlockMessages = errorBlock.find('#error-messages');
        if (getCookie(cookieName) !== undefined) {
            errorBlockMessages.html(`
                <ul>
                    <li>
                        К сожалению, Вы недавно оставляли отзыв! Попробуйте повторить через некоторое время.
                    </li>
                </ul>
            `);
            errorBlock.show();
            e.preventDefault();
        } else {
            errorBlock.hide();
            if (form !== null && form !== undefined) {
                for(let elem of form) {
                    let cleanValue = elem.value.trim().replace('.', null);
                    if (!cleanValue || !cleanValue.length || cleanValue.length < 2) {
                        errorBlockMessages.html(`
                        <ul>
                            <li>
                                Минимальная длина полей ввода: 2 символа
                            </li>
                            <li>
                                Введите Ваше имя и текст отзыва
                            </li>
                        </ul>
                    `);
                        errorBlock.show();
                        doSubmit = false;
                    }
                }
            }
            if (!doSubmit) {
                e.preventDefault();
            } else {
                setCookie(cookieName, 1, cookieOptions);
            }
        }
    });
});

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}