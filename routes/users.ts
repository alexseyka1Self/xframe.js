import * as express from "express";

const router = express.Router();

let users = [{id: 1, name: "Alex"}, {id: 2, name: "Serge"}, {id: 3, name: "Test"}];



/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send(users);
});

module.exports = router;
