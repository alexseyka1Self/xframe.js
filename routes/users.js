"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var router = express.Router();
var users = [{ id: 1, name: "Alex" }, { id: 2, name: "Serge" }, { id: 3, name: "Test" }];
/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send(users);
});
module.exports = router;
//# sourceMappingURL=users.js.map